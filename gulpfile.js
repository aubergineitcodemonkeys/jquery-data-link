/**
 * Created by dennislodder on 10/08/16.
 */
var gulp = require('gulp');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('dist', dist);

function dist() {
    gulp.src('./src/jquery.datalink.js')
        .pipe(uglify())
        .pipe(rename({'extname':'.min.js'}))
        .pipe(gulp.dest('./dist'));
}
