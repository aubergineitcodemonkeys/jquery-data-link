(function (jQuery) {
    "use strict";

    jQuery(document).ready(function () {
        jQuery(document).on('click', '[data-href]', onDataHrefClick);
        jQuery(document).on('click', '[data-link]', onDataLinkSelectorClick);

        function onDataHrefClick(e) {
            // Keep other clickable things available.
            if (['A', 'INPUT', 'BUTTON'].indexOf(e.target.nodeName) > -1) return true;

            window.location.href = this.getAttribute('data-href');
        }

        function onDataLinkSelectorClick(e) {
            // Keep other clickable things available.
            if (['A', 'INPUT', 'BUTTON'].indexOf(e.target.nodeName) > -1) return true;

            var selector = this.getAttribute('data-link');
            // Default to <a> tags when no value was given.
            if (selector == '') {
                selector = 'a';
            }

            // Try to find the given selector within the clicked element.
            var $link = jQuery(this).find(selector).first();
            if ($link.length == 0) return true;
            if (e.target == $link[0]) return true;

            //Simulate a click on the element that was found.
            $link[0].click();
        }
    });

})(jQuery);
