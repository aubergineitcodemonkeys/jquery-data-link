# Data Link javascript plugin.

## Installation

### Manual 
Download the `dist/jquery.data-link.min.js` and add it to your project. Make sure jQuery is available before the script is loaded.

## Usage

Add the data-href attribute to your html element to go to the given URL.
 
```html
<article data-href="https://yegor256.github.io/tacit/">Check out Tacit</article>
```

When you don't have access to the URL but you know a class or id of the element that contains the url, you can use a selector, don't forget to prefix it with # or . for id's and classes:
 
```html
<article data-link=".use-this-link">
    ...
    <a class="use-this-link" href="http://www.aubergine-it.nl">link</a>
    ...
</article>
```

If you don't know an id or class for your link, but you are pretty sure it is the only or first link within the element:
 
```html
<article data-link="a">...</article>
```

This is also the default if no value is given:
 
```html
<article data-link="">...</article>
```

See the example.html for a more comprehensive demo.
